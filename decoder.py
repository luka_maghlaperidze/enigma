def decode(text, code):
    words = ""
    for each in text:
        each = each.lower()
        num = ord(each)
        if num + code - 20 >= 0:
            num = num + code - 20
        words += chr(num)
    decoded_string = ""

    for i in words:
        num = ord(i) + code - 2
        decoded_string += chr(num)
    new_code = ""
    for j in str(code):
        new_code+=chr(ord(j)+3)
    print(decoded_string,new_code)
